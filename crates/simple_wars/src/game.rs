use color_eyre::eyre::Result as EyreResult;
use ggez::{
    event::EventHandler,
    graphics::{self, Color},
    winit::event::VirtualKeyCode,
    Context, GameError, GameResult,
};
use glam::Vec2;
use sw_core::{world::TileKind, GameWorld};
use utils::spritesheet;

use crate::{
    input::InputState,
    render::{tilemap::Tilemap, TileCamera, WorldRenderer},
};

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Speed {
    Normal,
    Fast,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum GameState {
    Paused,
    Invalid,
    Running(Speed),
}

impl Speed {
    pub fn should_tick(&self, delta: std::time::Duration) -> bool {
        match *self {
            Speed::Normal => delta > std::time::Duration::from_millis(500),
            Speed::Fast => delta > std::time::Duration::from_millis(200),
        }
    }
}

pub struct Game {
    camera: TileCamera,
    world_renderer: WorldRenderer,
    // gui_renderer: GuiRenderer,
    input_state: InputState,
    last_state_tick: std::time::Instant,
    game_state: GameState,
    pub game_world: GameWorld,
}

impl Game {
    pub fn new(ctx: &mut Context) -> EyreResult<Self> {
        ctx.gfx.add_font(
            "Tangerine_Regular",
            graphics::FontData::from_path(ctx, "/Tangerine_Regular.ttf")?,
        );

        let (w, h) = ctx.gfx.drawable_size();
        let mut camera = TileCamera::new(Vec2::from_slice(&[w, h]), 32.0);
        // camera.center_at_tile([world_size as f32 / 2.0, world_size as f32 / 2.0].into());
        camera.set_height(4);

        let world_size = 5;

        let tileset: spritesheet::Spritesheet<TileKind> =
            spritesheet::from_file(ctx, "/sprites.png", "/sprites.ron", 32.0);
        //
        // let gui_tileset = spritesheet::from_file(ctx, "/tiles/outdoors.png", "/tiles/sprites.ron");
        //
        let tiles = vec![None; (world_size * world_size) as usize];

        let tilemap = Tilemap::new(
            ctx,
            world_size,
            world_size,
            32.0,
            32.0,
            vec![tiles],
            tileset,
        );
        let world_renderer = WorldRenderer::new(tilemap);
        // let gui_renderer = GuiRenderer::new(ctx, gui_tileset)?;

        let mut entities = hecs::World::new();

        Ok(Game {
            world_renderer,
            // gui_renderer,
            camera,
            last_state_tick: std::time::Instant::now(),
            input_state: InputState::default(),
            game_state: GameState::Paused,
            game_world: GameWorld::new(5),
        })
    }

    // pub fn update_camera(&mut self) {
    //     if self
    //         .input_manager
    //         .input_state
    //         .just_pressed(VirtualKeyCode::Up)
    //     {
    //         self.camera.move_by([0.0, 16.0].into());
    //     }
    //     if self
    //         .input_manager
    //         .input_state
    //         .just_pressed(VirtualKeyCode::Down)
    //     {
    //         self.camera.move_by([0.0, -16.0].into());
    //     }
    //     if self
    //         .input_manager
    //         .input_state
    //         .just_pressed(VirtualKeyCode::Left)
    //     {
    //         self.camera.move_by([-16.0, 0.0].into());
    //     }
    //     if self
    //         .input_manager
    //         .input_state
    //         .just_pressed(VirtualKeyCode::Right)
    //     {
    //         self.camera.move_by([16.0, 0.0].into());
    //     }
    //     if self
    //         .input_manager
    //         .input_state
    //         .just_pressed(VirtualKeyCode::NumpadAdd)
    //         || self
    //             .input_manager
    //             .input_state
    //             .just_pressed(VirtualKeyCode::Plus)
    //     {
    //         self.camera.go_down();
    //     }
    //     if self
    //         .input_manager
    //         .input_state
    //         .just_pressed(VirtualKeyCode::NumpadSubtract)
    //         || self
    //             .input_manager
    //             .input_state
    //             .just_pressed(VirtualKeyCode::Minus)
    //     {
    //         self.camera.go_up();
    //     }
    //     if self
    //         .input_manager
    //         .input_state
    //         .just_pressed(VirtualKeyCode::P)
    //     {
    //         if self.game_state != GameState::Paused {
    //             self.game_state = GameState::Paused;
    //         } else {
    //             self.game_state = GameState::Running(Speed::Normal);
    //         }
    //     }
    //     if self
    //         .input_manager
    //         .input_state
    //         .just_pressed(VirtualKeyCode::LBracket)
    //         && self.game_state == GameState::Running(Speed::Fast)
    //     {
    //         self.game_state = GameState::Running(Speed::Normal);
    //     }
    //     if self
    //         .input_manager
    //         .input_state
    //         .just_pressed(VirtualKeyCode::RBracket)
    //         && self.game_state == GameState::Running(Speed::Normal)
    //     {
    //         self.game_state = GameState::Running(Speed::Fast);
    //     }
    // }

    fn handle_input(&mut self) -> EyreResult<()> {
        Ok(())
        //
        //     let mut commands = self.input_manager.get_commands(
        //         &self.game_world.world,
        //         &self.game_world.entities,
        //         &self.camera,
        //     );
        //
        //     if !commands.is_empty() {
        //         log::info!("Commands: {commands:?}");
        //     }
        //
        //     let mut state_cmds = vec![];
        //
        //     while let Some(command) = commands.pop() {
        //         match command {
        //             InputCommand::State(cmd) => state_cmds.push(cmd),
        //             InputCommand::Debug(DebugCommand::ToggleCollisionOverlay) => {
        //                 self.gui_renderer.collision_overlay = !self.gui_renderer.collision_overlay;
        //             }
        //             InputCommand::Debug(DebugCommand::DumpJobInfo) => {
        //                 let Some(jobs) = self.game_world.job_manager.iter::<DigJob>() else {
        //                     println!("No dig jobs found");
        //                     continue;
        //                 };
        //                 for job in jobs {
        //                     println!("{job:#?}");
        //                 }
        //             }
        //         }
        //     }
        //     if !state_cmds.is_empty() {
        //         self.game_world.handle_commands(state_cmds);
        //     }
        //     Ok(())
    }
}

impl EventHandler for Game {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        let now = std::time::Instant::now();

        if self.input_state.just_released(VirtualKeyCode::Escape) {
            ctx.request_quit();
        }

        self.handle_input()
            .map_err(|e| GameError::CustomError(format!("{e:?}")))?;

        match self.game_state {
            GameState::Paused => {}
            GameState::Invalid => {}
            GameState::Running(speed) if speed.should_tick(now - self.last_state_tick) => {
                // match self.game_world.update() {
                //     Ok(_) => {}
                //     Err(DemesneError::EntityNotFound) => {
                //         return Err(GameError::CustomError("Entity not Found".to_string()));
                //     }
                //     Err(DemesneError::InvalidState(msg)) => {
                //         self.game_state = GameState::Invalid;
                //         log::error!("State error: {msg}");
                //     }
                // };
                println!("Tick");
                self.last_state_tick = std::time::Instant::now();
            }
            GameState::Running(_) => {}
        }
        self.world_renderer
            .update(ctx, &self.game_world)
            .map_err(|e| GameError::CustomError(format!("{e:?}")))?;

        // self.gui_renderer
        //     .update(
        //         &height_grid,
        //         ctx,
        //         &self.game_world,
        //         &self.camera,
        //         &self.input_manager,
        //     )
        //     .map_err(|e| GameError::CustomError(format!("{e:?}")))?;
        //
        // self.update_camera();
        //
        // self.input_manager.reset();
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        let mut canvas = graphics::Canvas::from_frame(ctx, Color::from_rgb(50, 50, 50));

        self.world_renderer
            .render(&mut canvas, &self.camera)
            .map_err(|e| GameError::CustomError(format!("{e:?}")))?;
        // self.gui_renderer
        //     .render(&mut canvas, &self.camera, ctx)
        //     .map_err(|e| GameError::CustomError(format!("{e:?}")))?;

        canvas.finish(ctx)
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        input: ggez::input::keyboard::KeyInput,
        _repeated: bool,
    ) -> Result<(), GameError> {
        self.input_state.update_key_down_state(input);

        Ok(())
    }

    fn mouse_motion_event(
        &mut self,
        _ctx: &mut Context,
        x: f32,
        y: f32,
        _dx: f32,
        _dy: f32,
    ) -> GameResult<()> {
        // let screen_pos = Some([x, y].into());
        // self.input_manager.input_state.update_mouse_position(
        //     screen_pos,
        //     &self.game_world.world,
        //     &self.game_world.entities,
        //     &self.camera,
        // );
        Ok(())
    }

    fn resize_event(
        &mut self,
        _ctx: &mut Context,
        _width: f32,
        _height: f32,
    ) -> Result<(), GameError> {
        Ok(())
    }

    fn on_error(
        &mut self,
        _ctx: &mut Context,
        _origin: ggez::event::ErrorOrigin,
        _e: GameError,
    ) -> bool {
        true
    }

    fn mouse_button_down_event(
        &mut self,
        _ctx: &mut Context,
        button: ggez::event::MouseButton,
        _x: f32,
        _y: f32,
    ) -> Result<(), GameError> {
        // let mouse_button = match button {
        //     ggez::event::MouseButton::Left => MouseButton::Left,
        //     ggez::event::MouseButton::Right => MouseButton::Right,
        //     ggez::event::MouseButton::Middle => todo!(),
        //     ggez::event::MouseButton::Other(_) => todo!(),
        // };
        // self.input_manager
        //     .input_state
        //     .mouse
        //     .update_down_state(mouse_button);
        Ok(())
    }

    fn mouse_button_up_event(
        &mut self,
        _ctx: &mut Context,
        _button: ggez::event::MouseButton,
        _x: f32,
        _y: f32,
    ) -> Result<(), GameError> {
        Ok(())
    }

    fn mouse_enter_or_leave(
        &mut self,
        _ctx: &mut Context,
        _entered: bool,
    ) -> Result<(), GameError> {
        Ok(())
    }

    fn mouse_wheel_event(&mut self, _ctx: &mut Context, _x: f32, _y: f32) -> Result<(), GameError> {
        Ok(())
    }

    fn key_up_event(
        &mut self,
        _ctx: &mut Context,
        input: ggez::input::keyboard::KeyInput,
    ) -> Result<(), GameError> {
        self.input_state.update_key_up_state(input);
        Ok(())
    }

    fn quit_event(&mut self, _ctx: &mut Context) -> Result<bool, GameError> {
        log::debug!("quit_event() callback called, quitting...");
        Ok(false)
    }
}
