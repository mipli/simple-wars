use std::collections::HashSet;

use ggez::winit::event::VirtualKeyCode;
use glam::{IVec2, Vec2};
// use crate::render::TileCamera;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum MouseButton {
    Left,
    Right,
}

#[derive(Debug, Clone, Default)]
pub struct MousePosition {
    pub screen: Option<Vec2>,
    pub world: Option<Vec2>,
    pub tile: Option<IVec2>,
}

#[derive(Debug, Default)]
pub struct MouseState {
    pub position: MousePosition,
    just_pressed_set: HashSet<MouseButton>,
}

impl MouseState {
    pub fn update_down_state(&mut self, button: MouseButton) {
        self.just_pressed_set.insert(button);
    }

    pub fn just_pressed(&self, button: MouseButton) -> bool {
        self.just_pressed_set.contains(&button)
    }

    pub fn reset(&mut self) {
        self.just_pressed_set.clear();
    }
}

#[derive(Debug, Default)]
pub struct InputState {
    pub mouse: MouseState,
    just_pressed_set: HashSet<VirtualKeyCode>,
    just_released_set: HashSet<VirtualKeyCode>,
}

impl InputState {
    pub fn new() -> Self {
        InputState {
            mouse: MouseState::default(),
            just_pressed_set: HashSet::default(),
            just_released_set: HashSet::default(),
        }
    }

    pub fn update_mouse_position(
        &mut self,
        screen_pos: Option<Vec2>,
        // world: &World,
        entities: &hecs::World,
        // camera: &TileCamera,
    ) {
        // self.mouse.position.screen = screen_pos;
        // if let Some(pos) = self.mouse.position.screen {
        //     let world_pos =
        //         camera.screen_to_world_coords((pos.x.floor() as i32, pos.y.floor() as i32));
        //     self.mouse.position.world = Some(world_pos);
        //     self.mouse.position.tile = camera.world_to_tile_pos(world_pos, world, entities);
        // }
    }

    pub fn update_key_down_state(&mut self, input: ggez::input::keyboard::KeyInput) {
        if let Some(keycode) = input.keycode {
            self.just_pressed_set.insert(keycode);
        }
    }

    pub fn update_key_up_state(&mut self, input: ggez::input::keyboard::KeyInput) {
        if let Some(keycode) = input.keycode {
            self.just_pressed_set.remove(&keycode);
            self.just_released_set.insert(keycode);
        }
    }

    pub fn just_pressed(&self, key: VirtualKeyCode) -> bool {
        self.just_pressed_set.contains(&key)
    }

    pub fn just_released(&self, key: VirtualKeyCode) -> bool {
        self.just_released_set.contains(&key)
    }

    pub fn reset(&mut self) {
        self.just_pressed_set.clear();
        self.just_released_set.clear();
        self.mouse.reset();
    }
}
