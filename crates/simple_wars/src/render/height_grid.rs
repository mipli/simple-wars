use utils::grid::Grid;

pub struct HeightGrid {
    pub width: u32,
    pub depth: u32,
    cells: Vec<u32>,
}

impl HeightGrid {
    pub fn new(cells: Vec<u32>, width: u32, depth: u32) -> Self {
        HeightGrid {
            width,
            depth,
            cells,
        }
    }
}

impl Grid<u32> for HeightGrid {
    fn width(&self) -> u32 {
        self.width
    }

    fn depth(&self) -> u32 {
        self.depth
    }

    fn cells(&self) -> &[u32] {
        &self.cells
    }

    fn get(&self, x: i32, y: i32) -> Option<&u32> {
        let index = self.to_index(x, y);
        self.cells.get(index)
    }

    fn set(&mut self, x: i32, y: i32, tile: u32) {
        let index = self.to_index(x, y);
        self.cells[index] = tile;
    }
}
