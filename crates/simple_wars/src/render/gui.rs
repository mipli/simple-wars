use std::{collections::HashMap, hash::Hash};

use color_eyre::eyre::Result as EyreResult;
use demesne_core::{
    action::components::{Action, ActionQueue, DigAction, MoveAction},
    cache::CollisionState,
    components::{Edible, Position, Selected},
    hunger::Nutrition,
    job::{DigJob, JobOrder},
    state::GameWorld,
    world::Cell,
};
use egui::{Align, Direction, Layout};
use ggez::{graphics, Context};
use hecs::With;
use utils::{
    cube_map::CubeMap,
    grid::Grid,
    spritesheet::{SpriteKind, Spritesheet},
    tiles::TilePos,
};

use super::{height_grid::HeightGrid, TileCamera};
use crate::{
    input::{InputManager, Mode as InputMode, MousePosition},
    render::egui::*,
};

#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy)]
enum DrawMesh {
    MoveAction,
    DigAction,
    ActorSelection,
    EatAction,
    BlockedFull,
    BlockedMotion,
}

pub struct GuiRenderer {
    meshes: HashMap<DrawMesh, graphics::Mesh>,
    mouse_position: MousePosition,
    draw_queue: Vec<(TilePos, DrawMesh)>,
    egui_backend: EguiBackend,
    pub collision_overlay: bool,
    spritesheet: Spritesheet,
    tile_icon_mesh: graphics::Mesh,
    current_cell: Option<Cell>,
    input_mode: InputMode,
}

impl GuiRenderer {
    pub fn new(ctx: &mut Context, spritesheet: Spritesheet) -> EyreResult<Self> {
        let tile_icon_mesh = graphics::Mesh::new_rectangle(
            ctx,
            graphics::DrawMode::fill(),
            graphics::Rect::new(0.0, 0.0, 100.0, 100.0),
            graphics::Color::WHITE,
        )
        .unwrap();
        Ok(GuiRenderer {
            meshes: create_gui_meshes(ctx)?,
            mouse_position: MousePosition::default(),
            draw_queue: vec![],
            egui_backend: EguiBackend::new(ctx),
            collision_overlay: false,
            spritesheet,
            tile_icon_mesh,
            current_cell: None,
            input_mode: InputMode::Normal,
        })
    }
    pub fn update(
        &mut self,
        height_grid: &HeightGrid,
        ctx: &mut Context,
        state: &GameWorld,
        camera: &TileCamera,
        input_manager: &InputManager,
    ) -> EyreResult<()> {
        self.update_gui(ctx, state, camera, input_manager)?;
        self.update_world(height_grid, ctx, state, camera, input_manager)?;
        self.update_tile_icons(height_grid, ctx, state, camera, input_manager)?;
        self.input_mode = input_manager.mode();
        Ok(())
    }

    fn update_gui(
        &mut self,
        ctx: &mut Context,
        state: &GameWorld,
        camera: &TileCamera,
        _input_manager: &InputManager,
    ) -> EyreResult<()> {
        let egui_ctx = self.egui_backend.ctx();
        self.draw_left_panel(&egui_ctx, &state.entities);
        self.draw_top_panel(&egui_ctx, camera);
        self.draw_bottom_panel(&egui_ctx, camera);
        self.egui_backend.update(ctx);
        Ok(())
    }
    fn update_tile_icons(
        &mut self,
        height_grid: &HeightGrid,
        ctx: &mut Context,
        state: &GameWorld,
        camera: &TileCamera,
        _input_manager: &InputManager,
    ) -> EyreResult<()> {
        let sprite_id = self.spritesheet.get_sprite_id(SpriteKind::IconPickAxe);
        let sprite = self.spritesheet.get(sprite_id).unwrap();
        let src_rect = sprite.rect;
        let mut verts: Vec<graphics::Vertex> = vec![];
        let mut indices = vec![];
        let mut idx = 0;

        if let Some(order_iter) = state.job_manager.iter::<DigJob>() {
            for JobOrder {
                job: DigJob { target },
                ..
            } in order_iter
            {
                let z = *height_grid.get(target.x, target.y).unwrap() as i32;
                if target.z <= camera.get_height() && target.z >= z {
                    let dest_pt = glam::Vec2::from_slice(&[
                        (target.x as f32) * 16.0,
                        (target.y as f32) * 16.0,
                    ]);
                    let color = graphics::Color::WHITE;
                    let v = [
                        graphics::Vertex {
                            position: [dest_pt.x, dest_pt.y],
                            uv: [src_rect.x, src_rect.y],
                            color: color.into(),
                        },
                        graphics::Vertex {
                            position: [dest_pt.x + 16.0, dest_pt.y],
                            uv: [src_rect.x + src_rect.w, src_rect.y],
                            color: color.into(),
                        },
                        graphics::Vertex {
                            position: [dest_pt.x + 16.0, dest_pt.y + 16.0],
                            uv: [src_rect.x + src_rect.w, src_rect.y + src_rect.h],
                            color: color.into(),
                        },
                        graphics::Vertex {
                            position: [dest_pt.x, dest_pt.y + 16.0],
                            uv: [src_rect.x, src_rect.y + src_rect.h],
                            color: color.into(),
                        },
                    ];
                    verts.extend(v);
                    indices.extend([idx, idx + 1, idx + 2, idx + 2, idx + 3, idx]);
                    idx += 4;
                }
            }
        }
        self.tile_icon_mesh = graphics::Mesh::from_data(
            ctx,
            graphics::MeshData {
                vertices: verts.as_slice(),
                indices: indices.as_slice(),
            },
        );
        Ok(())
    }

    fn update_world(
        &mut self,
        height_grid: &HeightGrid,
        _ctx: &mut Context,
        state: &GameWorld,
        camera: &TileCamera,
        input_manager: &InputManager,
    ) -> EyreResult<()> {
        self.draw_queue.clear();
        let mut aq_query = state.entities.query::<&ActionQueue>();
        let aq_view = aq_query.view();

        for (id, Position(pos)) in state.entities.query::<With<&Position, &Selected>>().iter() {
            let z = *height_grid.get(pos.x, pos.y).unwrap() as i32;
            if pos.z <= camera.get_height() && pos.z >= z {
                self.draw_queue.push((*pos, DrawMesh::ActorSelection));
            }

            if let Some(action_queue) = aq_view.get(id) {
                let mut last_pos = *pos;
                for action in action_queue.iter() {
                    if let Some((target, mesh)) = match action {
                        Action::MoveAction(MoveAction { target }) => {
                            last_pos = target.0;
                            Some((target.0, DrawMesh::MoveAction))
                        }
                        Action::EatAction(_) => Some((last_pos, DrawMesh::EatAction)),
                        Action::DigAction(DigAction { target }) => {
                            last_pos = target.0;
                            Some((target.0, DrawMesh::DigAction))
                        }
                        Action::PassAction(_) => None,
                    } {
                        let z = *height_grid.get(target.x, target.y).unwrap() as i32;
                        if target.z <= camera.get_height() && target.z >= z {
                            self.draw_queue.push((target, mesh));
                        }
                    }
                }
            }
        }

        if self.collision_overlay {
            for x in 0..state.collision_cache.width() {
                for y in 0..state.collision_cache.depth() {
                    let pos: TilePos = (x, y, camera.get_height() as u32).into();
                    match state.collision_cache.get(pos) {
                        Some(CollisionState::Blocked) => {
                            self.draw_queue.push((pos, DrawMesh::BlockedFull))
                        }
                        Some(CollisionState::Mobile) => {
                            self.draw_queue.push((pos, DrawMesh::BlockedMotion))
                        }
                        _ => {}
                    }
                }
            }
        }
        self.mouse_position = input_manager.input_state.mouse.position.clone();
        self.current_cell = self
            .mouse_position
            .tile
            .and_then(|tile_pos| state.world.get(tile_pos))
            .cloned();

        Ok(())
    }

    pub fn render(
        &self,
        canvas: &mut graphics::Canvas,
        camera: &TileCamera,
        _ctx: &Context,
    ) -> EyreResult<()> {
        canvas.draw(&self.egui_backend, graphics::DrawParam::default());

        for (pos, draw_mesh) in &self.draw_queue {
            let dest = camera.world_to_screen_coords(*pos);
            if let Some(mesh) = self.meshes.get(draw_mesh) {
                canvas.draw(mesh, graphics::DrawParam::default().dest(dest));
            }
        }

        canvas.set_sampler(graphics::Sampler::nearest_clamp());
        canvas.draw_textured_mesh(
            self.tile_icon_mesh.clone(),
            self.spritesheet.image.clone(),
            graphics::DrawParam::new().dest(camera.draw_offset()),
        );

        Ok(())
    }

    fn draw_left_panel(&self, egui_ctx: &EguiContext, entities: &hecs::World) {
        egui::SidePanel::left("left panel").show(egui_ctx, |ui| {
            let mut nutrition_query = entities.query::<&Nutrition>();
            let nutrition_view = nutrition_query.view();

            let mut aq_query = entities.query::<&ActionQueue>();
            let aq_view = aq_query.view();

            let mut edible_query = entities.query::<&Edible>();
            let edible_view = edible_query.view();

            for (id, Position(pos)) in entities.query::<With<&Position, &Selected>>().iter() {
                ui.add(egui::Label::new(format!("ID: {id:?}")));
                ui.add(egui::Label::new(format!("@{pos}")));

                if let Some(nutrition) = nutrition_view.get(id) {
                    ui.add(egui::Label::new(format!("Hunger: {}", nutrition.satedness)));
                }
                if let Some(edible) = edible_view.get(id) {
                    ui.add(egui::Label::new(format!("Nutrition: {}", edible.nutrition)));
                }
                if let Some(aq) = aq_view.get(id) {
                    ui.separator();
                    for action in aq.iter().take(3) {
                        ui.add(egui::Label::new(format!("{action}")));
                    }
                    let cnt = aq.iter().count();
                    if cnt > 3 {
                        if cnt > 4 {
                            ui.add(egui::Label::new("..."));
                        }
                        ui.add(egui::Label::new(format!(
                            "{}",
                            aq.iter()
                                .last()
                                .expect("We know there are items in the iterator")
                        )));
                    }
                }
            }
        });
    }

    fn draw_top_panel(&self, egui_ctx: &EguiContext, camera: &TileCamera) {
        let text = format!("{} - View level: {}", self.input_mode, camera.get_height());
        egui::TopBottomPanel::top("top panel")
            .frame(
                egui::Frame::none()
                    .fill(egui::Color32::TRANSPARENT)
                    .inner_margin(5.0),
            )
            .show(egui_ctx, |ui| {
                ui.allocate_ui(ui.available_size_before_wrap(), |ui| {
                    ui.with_layout(
                        Layout::from_main_dir_and_cross_align(Direction::TopDown, Align::RIGHT)
                            .with_main_wrap(false)
                            .with_cross_justify(false),
                        |ui| ui.add(egui::Label::new(text)),
                    );
                });
            });
    }

    fn draw_bottom_panel(&self, egui_ctx: &EguiContext, _camera: &TileCamera) {
        let pos_text = match self.mouse_position.tile {
            Some(tile) => format!("[{}, {}, {}]", tile.x, tile.y, tile.z,),
            _ => "[out of bounds]".to_string(),
        };
        let tile_text = self.current_cell.as_ref().map(|cell| {
            let properties = cell
                .properties
                .iter()
                .map(|prop| format!("{prop}"))
                .collect::<Vec<_>>();

            if !properties.is_empty() {
                format!("{}\n{}", properties.join("\n"), cell)
            } else {
                format!("{cell}")
            }
        });
        egui::TopBottomPanel::bottom("bottom panel")
            .frame(
                egui::Frame::none()
                    .fill(egui::Color32::TRANSPARENT)
                    .inner_margin(5.0),
            )
            .show(egui_ctx, |ui| {
                ui.allocate_ui(ui.available_size_before_wrap(), |ui| {
                    ui.with_layout(
                        Layout::from_main_dir_and_cross_align(Direction::TopDown, Align::RIGHT)
                            .with_main_wrap(false)
                            .with_cross_justify(false),
                        |ui| {
                            ui.add(egui::Label::new(pos_text));
                            if let Some(text) = tile_text {
                                ui.add(egui::Label::new(text));
                            }
                        },
                    );
                });
            });
    }
}

fn create_gui_meshes(ctx: &ggez::Context) -> EyreResult<HashMap<DrawMesh, graphics::Mesh>> {
    let mb = &mut graphics::MeshBuilder::new();
    mb.rectangle(
        graphics::DrawMode::stroke(1.0),
        graphics::Rect::new(0.0, 0.0, 16.0, 16.0),
        graphics::Color::new(0.8, 0.8, 0.8, 0.8),
    )?;
    let select_mesh = graphics::Mesh::from_data(ctx, mb.build());

    let mb = &mut graphics::MeshBuilder::new();
    mb.rectangle(
        graphics::DrawMode::stroke(1.0),
        graphics::Rect::new(4.0, 4.0, 8.0, 8.0),
        graphics::Color::new(1.0, 1.0, 1.0, 0.4),
    )?;
    let move_mesh = graphics::Mesh::from_data(ctx, mb.build());

    let mb = &mut graphics::MeshBuilder::new();
    mb.rectangle(
        graphics::DrawMode::stroke(1.0),
        graphics::Rect::new(7.0, 7.0, 2.0, 2.0),
        graphics::Color::new(1.0, 1.0, 1.0, 0.4),
    )?;
    let eat_mesh = graphics::Mesh::from_data(ctx, mb.build());

    let mb = &mut graphics::MeshBuilder::new();
    mb.rectangle(
        graphics::DrawMode::fill(),
        graphics::Rect::new(0.0, 0.0, 16.0, 16.0),
        graphics::Color::new(0.7, 0.0, 0.0, 0.3),
    )?;
    let block_full_mesh = graphics::Mesh::from_data(ctx, mb.build());

    let mb = &mut graphics::MeshBuilder::new();
    mb.rectangle(
        graphics::DrawMode::fill(),
        graphics::Rect::new(0.0, 0.0, 16.0, 16.0),
        graphics::Color::new(0.6, 0.2, 0.2, 0.3),
    )?;
    let block_mobile_mesh = graphics::Mesh::from_data(ctx, mb.build());

    let mb = &mut graphics::MeshBuilder::new();
    mb.rectangle(
        graphics::DrawMode::stroke(1.0),
        graphics::Rect::new(7.0, 7.0, 2.0, 2.0),
        graphics::Color::new(0.0, 7.0, 1.0, 0.8),
    )?;
    let dig_mesh = graphics::Mesh::from_data(ctx, mb.build());

    let mut meshes = HashMap::default();

    meshes.insert(DrawMesh::ActorSelection, select_mesh);
    meshes.insert(DrawMesh::MoveAction, move_mesh);
    meshes.insert(DrawMesh::DigAction, dig_mesh);
    meshes.insert(DrawMesh::EatAction, eat_mesh);
    meshes.insert(DrawMesh::BlockedFull, block_full_mesh);
    meshes.insert(DrawMesh::BlockedMotion, block_mobile_mesh);
    Ok(meshes)
}
