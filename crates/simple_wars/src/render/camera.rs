use glam::{IVec2, Vec2};

pub struct TileCamera {
    tile_size: f32,
    screen_size: Vec2,
    view_center: Vec2,
    height_focus: i32,
    max_height: i32,
}

impl TileCamera {
    pub fn new(screen_size: Vec2, tile_size: f32) -> Self {
        TileCamera {
            tile_size,
            screen_size,
            view_center: Vec2::new(0.0, 0.0),
            height_focus: 0,
            max_height: 32,
        }
    }

    pub fn move_by(&mut self, by: Vec2) {
        self.view_center.x += by.x;
        self.view_center.y += by.y;
    }

    pub fn go_up(&mut self) {
        if self.height_focus < self.max_height {
            self.height_focus += 1;
        }
    }

    pub fn go_down(&mut self) {
        if self.height_focus > 0 {
            self.height_focus -= 1;
        }
    }

    pub fn get_height(&self) -> i32 {
        self.height_focus
    }

    pub fn set_height(&mut self, height: i32) {
        if height > 0 && height <= self.max_height {
            self.height_focus = height;
        }
    }

    pub fn center_at_tile(&mut self, to: Vec2) {
        self.view_center = to * -self.tile_size
    }

    pub fn draw_offset(&self) -> Vec2 {
        self.view_center + Vec2::from_slice(&[self.screen_size.x / 2.0, self.screen_size.y / 2.0])
    }

    pub fn world_to_screen_coords(&self, from: IVec2) -> Vec2 {
        let offset: Vec2 = (
            from.x as f32 * self.tile_size,
            from.y as f32 * self.tile_size,
        )
            .into();
        offset + self.draw_offset()
    }

    // pub fn world_to_tile_pos(
    //     &self,
    //     from: Vec2,
    //     world: &World,
    //     entities: &hecs::World,
    // ) -> Option<IVec2> {
    //     let x = (from.x / 16.0).floor() as i32;
    //     let y = (from.y / 16.0).floor() as i32;
    //
    //     self.raycast_tile_or_entity((x, y, self.height_focus).into(), world, entities)
    // }

    pub fn screen_to_world_coords(&self, from: (i32, i32)) -> Vec2 {
        let (sx, sy) = from;
        let tx = sx as f32 - (self.screen_size.x / 2.0);
        let ty = sy as f32 - (self.screen_size.y / 2.0);
        let screen_coords = Vec2::new(tx, ty);
        screen_coords - self.view_center
    }

    // fn raycast_tile_or_entity(
    //     &self,
    //     pos: IVec2,
    //     world: &World,
    //     entities: &hecs::World,
    // ) -> Option<IVec2> {
    //     if !world.is_valid_position(pos) {
    //         return None;
    //     }
    //     let matching_entities = entities
    //         .query::<&Position>()
    //         .into_iter()
    //         .filter_map(|(_, Position(p))| {
    //             if p.x == pos.x && p.y == pos.y && p.z <= pos.z {
    //                 Some(*p)
    //             } else {
    //                 None
    //             }
    //         })
    //         .collect::<Vec<_>>();
    //     for h in (0..=pos.z).rev() {
    //         let test_pos: IVec2 = (pos.x, pos.y, h).into();
    //         if matching_entities.contains(&test_pos) {
    //             return Some(test_pos);
    //         }
    //         if let Some(cell) = world.get(test_pos) {
    //             match cell.kind {
    //                 CellKind::Air => {}
    //                 CellKind::Dirt => return Some((pos.x, pos.y, h).into()),
    //                 CellKind::Water => return Some((pos.x, pos.y, h).into()),
    //             }
    //         }
    //     }
    //     None
    // }
}
