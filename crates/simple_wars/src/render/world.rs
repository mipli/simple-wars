use color_eyre::eyre::Result as EyreResult;
use ggez::{graphics, Context};
use sw_core::{
    components::{DrawLayer, Drawable, Position},
    world::TileKind,
    GameWorld,
};

use crate::render::{
    tilemap::{TileEffect, Tilemap},
    TileCamera,
};

pub struct WorldRenderer {
    tilemap: Tilemap<TileKind>,
}

impl WorldRenderer {
    pub fn new(tilemap: Tilemap<TileKind>) -> Self {
        WorldRenderer { tilemap }
    }

    pub fn update(&mut self, ctx: &mut Context, game_world: &GameWorld) -> EyreResult<()> {
        let tiles: Vec<_> = game_world
            .world
            .tiles
            .iter()
            .enumerate()
            .map(|(index, tile)| {
                let pos = game_world.world.revert_index(index);
                let neigbours = game_world.world.get_all_neigbours(pos);

                let sprite_id = self.tilemap.spritesheet.get_auto_tile_id(
                    *tile, // TileKind::Grass,
                    &neigbours,
                );

                Some(sprite_id.into())
            })
            .collect();

        let mut entity_layer = vec![None; tiles.len()];
        let mut actor_layer = vec![None; tiles.len()];
        for (_, (Position { coordinate }, drawable)) in
            game_world.entities.query::<(&Position, &Drawable)>().iter()
        {
            let tile_index = coordinate.y * game_world.world.width as i32 + coordinate.x;
            let sprite = self.tilemap.spritesheet.get_sprite_id(drawable.sprite);
            // let effect = match camera.get_height() - pos.z {
            //     0 => TileEffect::None,
            //     diff => {
            //         let dim = dimming_ratio(diff as f32);
            //         TileEffect::Dim(dim)
            //     }
            // };
            let effect = TileEffect::None;
            match drawable.layer {
                DrawLayer::Entity => {
                    entity_layer[tile_index as usize] = Some((sprite, effect).into())
                }
                DrawLayer::Unit => actor_layer[tile_index as usize] = Some((sprite, effect).into()),
            }
        }

        if !tiles.is_empty() {
            self.tilemap.layers = vec![tiles.into(), entity_layer.into(), actor_layer.into()];
            self.tilemap.update(ctx);
        }

        Ok(())
    }

    pub fn render(&self, canvas: &mut graphics::Canvas, camera: &TileCamera) -> EyreResult<()> {
        canvas.set_sampler(graphics::Sampler::nearest_clamp());
        canvas.draw(
            &self.tilemap,
            graphics::DrawParam::new().dest(camera.draw_offset()),
        );

        Ok(())
    }
}

/// Calculate how much we should dim a tile that's lower than the current camera focus
fn dimming_ratio(height_diff: f32) -> f32 {
    let dim = (height_diff / 5.0).clamp(0.1, 0.9);
    1.0 - dim
}

#[cfg(test)]
mod tests {
    // use float_eq::assert_float_eq;
    //
    // use super::dimming_ratio;
    //
    // #[test]
    // fn test_dimming_ratio() {
    //     assert_float_eq!(dimming_ratio(1.0), 0.8, abs <= 0.001);
    //     assert_float_eq!(dimming_ratio(2.0), 0.6, abs <= 0.001);
    //     assert_float_eq!(dimming_ratio(3.0), 0.4, abs <= 0.001);
    //     assert_float_eq!(dimming_ratio(10.0), 0.1, abs <= 0.001);
    //     assert_float_eq!(dimming_ratio(15.0), 0.1, abs <= 0.001);
    // }
}
