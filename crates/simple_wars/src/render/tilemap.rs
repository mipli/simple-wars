use ggez::{self, graphics};
use utils::spritesheet::*;

/// Modify tile rendering
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum TileEffect {
    Dim(f32),
    None,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Tile(SpriteId, TileEffect);

impl From<SpriteId> for Tile {
    fn from(sprite_id: SpriteId) -> Self {
        Tile(sprite_id, TileEffect::None)
    }
}

impl From<(SpriteId, TileEffect)> for Tile {
    fn from((sprite_id, effect): (SpriteId, TileEffect)) -> Self {
        Tile(sprite_id, effect)
    }
}

/// A single layer in the map.
/// Each item is a source rect, or None
/// if there is nothing to be drawn for that location,
/// which makes life a lot simpler when drawing layered maps.
///
/// Tiles are stored in row-major order.
#[derive(Clone, Debug)]
pub struct Layer {
    pub tiles: Vec<Option<Tile>>,
}

impl From<Vec<Option<Tile>>> for Layer {
    fn from(tiles: Vec<Option<Tile>>) -> Self {
        Layer { tiles }
    }
}

impl Layer {
    /// Returns the tile ID at the given coordinate.
    fn get_tile(&self, x: usize, y: usize, width: usize) -> Option<Tile> {
        let offset = (y * width) + x;
        self.tiles[offset]
    }
}

#[derive(Clone, Debug)]
pub struct Tilemap<S: SpriteKindTrait> {
    pub layers: Vec<Layer>,
    width: usize,
    height: usize,
    tile_width: f32,
    tile_height: f32,
    pub spritesheet: Spritesheet<S>,
    mesh: graphics::Mesh,
}

impl<S: SpriteKindTrait> Tilemap<S> {
    pub fn new(
        ctx: &mut ggez::Context,
        width: usize,
        height: usize,
        tile_width: f32,
        tile_height: f32,
        layers: Vec<Vec<Option<Tile>>>,
        spritesheet: Spritesheet<S>,
    ) -> Self {
        let layers: Vec<Layer> = layers
            .into_iter()
            .map(|l| {
                // Ensure all layers are the right size.
                assert_eq!(width * height, l.len());
                Layer { tiles: l }
            })
            .collect();

        // default mesh, overridden by batch_layers
        let mesh = graphics::Mesh::new_rectangle(
            ctx,
            graphics::DrawMode::fill(),
            graphics::Rect::new(0.0, 0.0, 100.0, 100.0),
            graphics::Color::WHITE,
        )
        .unwrap();
        let mut s = Self {
            layers,
            width,
            height,

            tile_width,
            tile_height,
            spritesheet,
            mesh,
        };
        s.batch_layers(ctx);
        s
    }

    pub fn update(&mut self, ctx: &mut ggez::Context) {
        self.batch_layers(ctx)
    }

    fn batch_layers(&mut self, ctx: &mut ggez::Context) {
        let mut verts: Vec<graphics::Vertex> = vec![];
        let mut indices = vec![];
        let mut idx = 0;

        for y in 0..self.height {
            for x in 0..self.width {
                let first_opaque_layer = self.first_opaque_layer_at(x, y);
                for layer in &self.layers[first_opaque_layer..] {
                    if let Some(Tile(sprite_id, tile_effect)) = layer.get_tile(x, y, self.width) {
                        let sprite = self.spritesheet.get(sprite_id).expect("Invalid Tile ID");
                        let src_rect = sprite.rect;
                        let dest_pt = glam::Vec2::from_slice(&[
                            (x as f32) * self.tile_width,
                            (y as f32) * self.tile_height,
                        ]);
                        let color = match tile_effect {
                            TileEffect::None => graphics::Color::WHITE,
                            TileEffect::Dim(v) => {
                                assert!((0.0..=1.0).contains(&v));
                                let color = (255f32 * v).floor() as u8;
                                graphics::Color::from_rgb(color, color, color)
                            }
                        };
                        let v = [
                            graphics::Vertex {
                                position: [dest_pt.x, dest_pt.y],
                                uv: [src_rect.x, src_rect.y],
                                color: color.into(),
                            },
                            graphics::Vertex {
                                position: [dest_pt.x + self.tile_width, dest_pt.y],
                                uv: [src_rect.x + src_rect.w, src_rect.y],
                                color: color.into(),
                            },
                            graphics::Vertex {
                                position: [
                                    dest_pt.x + self.tile_width,
                                    dest_pt.y + self.tile_height,
                                ],
                                uv: [src_rect.x + src_rect.w, src_rect.y + src_rect.h],
                                color: color.into(),
                            },
                            graphics::Vertex {
                                position: [dest_pt.x, dest_pt.y + self.tile_height],
                                uv: [src_rect.x, src_rect.y + src_rect.h],
                                color: color.into(),
                            },
                        ];
                        verts.extend(v);
                        indices.extend([idx, idx + 1, idx + 2, idx + 2, idx + 3, idx]);
                        idx += 4;
                    }
                }
            }
        }
        self.mesh = graphics::Mesh::from_data(
            ctx,
            graphics::MeshData {
                vertices: verts.as_slice(),
                indices: indices.as_slice(),
            },
        )
    }

    fn first_opaque_layer_at(&self, x: usize, y: usize) -> usize {
        assert!(!self.layers.is_empty());
        for i in (0..self.layers.len()).rev() {
            if let Some(Tile(sprite_id, _)) = self.layers[i].get_tile(x, y, self.width) {
                let sprite = self.spritesheet.get(sprite_id).expect("Invalid tile ID!");
                if sprite.transparency == Transparency::Opaque {
                    return i;
                }
            }
        }
        0
    }
}

impl<S: SpriteKindTrait> graphics::Drawable for Tilemap<S> {
    fn draw(&self, canvas: &mut graphics::Canvas, param: impl Into<graphics::DrawParam>) {
        canvas.draw_textured_mesh(self.mesh.clone(), self.spritesheet.image.clone(), param);
    }

    fn dimensions(
        &self,
        gfx: &impl ggez::context::Has<graphics::GraphicsContext>,
    ) -> Option<graphics::Rect> {
        self.mesh.dimensions(gfx)
    }
}
