use std::{
    cell::RefCell,
    collections::{HashMap, LinkedList},
    ops::Deref,
    rc::Rc,
    time::Instant,
};

use egui::{pos2, vec2, Color32, Key, PointerButton, Pos2, RawInput, Stroke};
use ggez::{
    event::MouseButton,
    graphics::{self, Drawable},
    input::keyboard::{KeyCode, KeyMods},
};

/// Contains a copy of [`egui::Context`] and a mutable reference for the paint_jobs vector from [`Painter`].
///
/// When is droped automatically will call [`egui::Context::end_frame`] function and update the paint_jobs
pub struct EguiContext {
    context: egui::Context,
    painter: Rc<RefCell<Painter>>,
    clipboard: Rc<RefCell<String>>,
}

impl Deref for EguiContext {
    type Target = egui::Context;
    fn deref(&self) -> &Self::Target {
        &self.context
    }
}

impl Drop for EguiContext {
    fn drop(&mut self) {
        let egui::FullOutput {
            platform_output,
            textures_delta,
            shapes,
            ..
        } = self.context.end_frame();

        if !platform_output.copied_text.is_empty() {
            *self.clipboard.borrow_mut() = platform_output.copied_text;
        }
        self.painter.borrow_mut().shapes = self.context.tessellate(shapes);
        self.painter
            .borrow_mut()
            .textures_delta
            .push_front(textures_delta);
    }
}

/// Contains and handles everything related to [`egui`]
#[derive(Default)]
pub struct EguiBackend {
    context: egui::Context,
    pub input: Input,
    painter: Rc<RefCell<Painter>>,
}

impl EguiBackend {
    /// Create a [`EguiBackend`] with extra information for use the [`Input::set_scale_factor`]
    pub fn new(ctx: &ggez::Context) -> Self {
        let mut input = Input::default();
        let (w, h) = ctx.gfx.size();
        input.set_scale_factor(1.0, (w, h));
        let context = egui::Context::default();

        let mut style: egui::Style = (*context.style()).clone();
        style.visuals.override_text_color = Some(Color32::LIGHT_GRAY);
        style.visuals.widgets.noninteractive.bg_fill = Color32::DARK_GRAY;
        style.visuals.widgets.noninteractive.bg_stroke = Stroke::default();
        style.visuals.widgets.noninteractive.fg_stroke = Stroke::default();
        context.set_style(style);

        Self {
            input,
            context,
            ..Default::default()
        }
    }

    pub fn update(&mut self, ctx: &mut ggez::Context) {
        self.input.update(ctx);
        self.painter.borrow_mut().update(ctx);
    }

    /// Return an [`EguiContext`] for update the gui
    pub fn ctx(&mut self) -> EguiContext {
        self.context.begin_frame(self.input.take());
        EguiContext {
            context: self.context.clone(),
            painter: self.painter.clone(),
            clipboard: self.input.clipboard.clone(),
        }
    }
}

impl Drawable for EguiBackend {
    fn draw(&self, canvas: &mut graphics::Canvas, _param: impl Into<graphics::DrawParam>) {
        self.painter
            .borrow_mut()
            .draw(canvas, self.input.scale_factor);
    }

    fn dimensions(
        &self,
        _gfx: &impl ggez::context::Has<graphics::GraphicsContext>,
    ) -> Option<graphics::Rect> {
        None
    }
}

#[derive(Default, Clone)]
pub struct Painter {
    pub(crate) shapes: Vec<egui::ClippedPrimitive>,
    pub(crate) textures_delta: LinkedList<egui::TexturesDelta>,
    paint_jobs: Vec<(egui::TextureId, graphics::Mesh)>,
    textures: HashMap<egui::TextureId, graphics::Image>,
}

impl Painter {
    pub fn draw(&mut self, canvas: &mut graphics::Canvas, scale_factor: f32) {
        for (id, mesh) in self.paint_jobs.iter() {
            canvas.draw_textured_mesh(
                mesh.clone(),
                self.textures[id].clone(),
                graphics::DrawParam::default().scale([scale_factor, scale_factor]),
            );
        }
        self.paint_jobs.clear();
    }

    pub fn update(&mut self, ctx: &mut ggez::Context) {
        // Create and free textures
        if let Some(textures_delta) = self.textures_delta.pop_front() {
            self.update_textures(ctx, textures_delta);
        }

        // generating meshes
        for egui::ClippedPrimitive { primitive, .. } in self.shapes.iter() {
            match primitive {
                egui::epaint::Primitive::Mesh(mesh) => {
                    if mesh.vertices.len() < 3 {
                        continue;
                    }

                    let vertices = mesh
                        .vertices
                        .iter()
                        .map(|v| graphics::Vertex {
                            position: [v.pos.x, v.pos.y],
                            uv: [v.uv.x, v.uv.y],
                            color: egui::Rgba::from(v.color).to_array(),
                        })
                        .collect::<Vec<_>>();

                    self.paint_jobs.push((
                        mesh.texture_id,
                        graphics::Mesh::from_data(
                            ctx,
                            graphics::MeshData {
                                vertices: vertices.as_slice(),
                                indices: mesh.indices.as_slice(),
                            },
                        ),
                    ));
                }
                egui::epaint::Primitive::Callback(_) => {
                    todo!("Custom rendering callbacks are not implemented yet");
                }
            }
        }
    }

    pub fn update_textures(
        &mut self,
        ctx: &mut ggez::Context,
        textures_delta: egui::TexturesDelta,
    ) {
        // set textures
        for (id, delta) in &textures_delta.set {
            let image = match &delta.image {
                egui::ImageData::Color(image) => image.to_image(ctx),
                egui::ImageData::Font(image) => image.to_image(ctx),
            };

            self.textures.insert(*id, image);
        }

        // free textures
        for id in &textures_delta.free {
            self.textures.remove(id);
        }
    }
}

// Generate ggez Image from egui Texture
trait Image {
    fn to_image(&self, ctx: &mut ggez::Context) -> graphics::Image;
}

impl Image for egui::ColorImage {
    fn to_image(&self, ctx: &mut ggez::Context) -> graphics::Image {
        assert_eq!(
            self.width() * self.height(),
            self.pixels.len(),
            "Mismatch between texture size and texel count"
        );

        let mut pixels: Vec<u8> = Vec::with_capacity(self.pixels.len() * 4);

        for pixel in &self.pixels {
            pixels.extend(pixel.to_array());
        }

        graphics::Image::from_pixels(
            ctx,
            pixels.as_slice(),
            graphics::ImageFormat::Rgba8UnormSrgb,
            self.width() as u32,
            self.height() as u32,
        )
    }
}

impl Image for egui::FontImage {
    fn to_image(&self, ctx: &mut ggez::Context) -> graphics::Image {
        assert_eq!(
            self.width() * self.height(),
            self.pixels.len(),
            "Mismatch between texture size and texel count"
        );

        let mut pixels: Vec<u8> = Vec::with_capacity(self.pixels.len() * 4);

        let gamma = 1.0;
        for pixel in self.srgba_pixels(gamma) {
            pixels.extend(pixel.to_array());
        }

        graphics::Image::from_pixels(
            ctx,
            pixels.as_slice(),
            graphics::ImageFormat::Rgba8UnormSrgb,
            self.width() as u32,
            self.height() as u32,
        )
    }
}

/// Contains and manages everything related to the [`egui`] input
///
/// such as the location of the mouse or the pressed keys
pub struct Input {
    dt: Instant,
    pointer_pos: Pos2,
    pub(crate) raw: RawInput,
    pub(crate) scale_factor: f32,
    pub clipboard: Rc<RefCell<String>>,
}

impl Default for Input {
    /// scale_factor: 1.0
    fn default() -> Self {
        Self {
            dt: Instant::now(),
            pointer_pos: Default::default(),
            raw: Default::default(),
            scale_factor: 1.0,
            clipboard: Default::default(),
        }
    }
}

impl Input {
    pub(crate) fn take(&mut self) -> RawInput {
        self.raw.predicted_dt = self.dt.elapsed().as_secs_f32();
        self.dt = Instant::now();
        self.raw.take()
    }

    /// It updates egui of what is happening in the input (keys pressed, mouse position, etc), but it doesn't updates
    /// the information of the pressed characters, to update that information you have to
    /// use the function [text_input_event](Input:: text_input_event)
    pub fn update(&mut self, ctx: &ggez::Context) {
        /*======================= Keyboard =======================*/
        for key in ctx.keyboard.pressed_keys() {
            if let Some(key) = translate_keycode(*key) {
                self.raw.events.push(egui::Event::Key {
                    key,
                    pressed: true,
                    modifiers: translate_modifier(ctx.keyboard.active_mods()),
                })
            }
        }

        /*======================= Mouse =======================*/
        for button in [MouseButton::Left, MouseButton::Middle, MouseButton::Right] {
            if ctx.mouse.button_just_pressed(button) {
                self.raw.events.push(egui::Event::PointerButton {
                    button: match button {
                        MouseButton::Left => PointerButton::Primary,
                        MouseButton::Right => PointerButton::Secondary,
                        MouseButton::Middle => PointerButton::Middle,
                        _ => unreachable!(),
                    },
                    pos: self.pointer_pos,
                    pressed: true,
                    modifiers: translate_modifier(ctx.keyboard.active_mods()),
                });
            }
            if ctx.mouse.button_just_released(button) {
                self.raw.events.push(egui::Event::PointerButton {
                    button: match button {
                        MouseButton::Left => PointerButton::Primary,
                        MouseButton::Right => PointerButton::Secondary,
                        MouseButton::Middle => PointerButton::Middle,
                        _ => unreachable!(),
                    },
                    pos: self.pointer_pos,
                    pressed: false,
                    modifiers: translate_modifier(ctx.keyboard.active_mods()),
                });
            }
        }

        if ctx.mouse.delta() != [0.0, 0.0].into() {
            let ggez::mint::Point2 { x, y } = ctx.mouse.position();
            self.pointer_pos = pos2(x / self.scale_factor, y / self.scale_factor);
            self.raw
                .events
                .push(egui::Event::PointerMoved(self.pointer_pos));
        }
    }

    /// Set the scale_factor and update the screen_rect
    pub fn set_scale_factor(&mut self, scale_factor: f32, (w, h): (f32, f32)) {
        self.scale_factor = scale_factor;
        self.raw.pixels_per_point = Some(scale_factor);
        self.resize_event(w, h);
    }

    /// Update screen_rect data with window size
    pub fn resize_event(&mut self, w: f32, h: f32) {
        self.raw.screen_rect = Some(egui::Rect::from_min_size(
            Default::default(),
            vec2(w, h) / self.scale_factor,
        ));
    }
}

#[inline]
fn translate_keycode(key: KeyCode) -> Option<egui::Key> {
    Some(match key {
        KeyCode::Escape => Key::Escape,
        KeyCode::Insert => Key::Insert,
        KeyCode::Home => Key::Home,
        KeyCode::Delete => Key::Delete,
        KeyCode::End => Key::End,
        KeyCode::PageDown => Key::PageDown,
        KeyCode::PageUp => Key::PageUp,
        KeyCode::Left => Key::ArrowLeft,
        KeyCode::Up => Key::ArrowUp,
        KeyCode::Right => Key::ArrowRight,
        KeyCode::Down => Key::ArrowDown,
        KeyCode::Back => Key::Backspace,
        KeyCode::Return => Key::Enter,
        KeyCode::Tab => Key::Tab,
        KeyCode::Space => Key::Space,

        KeyCode::A => Key::A,
        KeyCode::B => Key::B,
        KeyCode::C => Key::C,
        KeyCode::D => Key::D,
        KeyCode::E => Key::E,

        _ => {
            return None;
        }
    })
}

#[inline]
fn translate_modifier(keymods: KeyMods) -> egui::Modifiers {
    egui::Modifiers {
        alt: keymods.intersects(KeyMods::ALT),
        ctrl: keymods.intersects(KeyMods::CTRL),
        shift: keymods.intersects(KeyMods::SHIFT),

        #[cfg(not(target_os = "macos"))]
        mac_cmd: false,
        #[cfg(not(target_os = "macos"))]
        command: keymods.intersects(KeyMods::CTRL),

        #[cfg(target_os = "macos")]
        mac_cmd: keymods.intersects(KeyMods::LOGO),
        #[cfg(target_os = "macos")]
        command: keymods.intersects(KeyMods::LOGO),
    }
}
