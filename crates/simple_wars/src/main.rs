use std::{env, path};

use clap::Parser;
use fern::colors::{Color, ColoredLevelConfig};
use ggez::{event, ContextBuilder};

mod game;
mod input;
mod render;
//
use color_eyre::eyre::Result as EyreResult;
use game::Game;
// use utils::cube_map::CubeMap;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Name of the person to greet
    #[arg(long)]
    map_dump: bool,
}

fn main() -> EyreResult<()> {
    color_eyre::install()?;

    let colors = ColoredLevelConfig::new().debug(Color::Green);
    fern::Dispatch::new()
        .level(log::LevelFilter::Error)
        .level_for("demesne", log::LevelFilter::Debug)
        .level_for("demesne_core", log::LevelFilter::Debug)
        .level_for("utils", log::LevelFilter::Debug)
        .level_for("hive", log::LevelFilter::Debug)
        .level_for("genesis", log::LevelFilter::Debug)
        .chain(
            fern::Dispatch::new()
                .format(move |out, message, record| {
                    out.finish(format_args!(
                        "{color_line}[{target}][{level}{color_line}] {message}\x1B[0m",
                        color_line =
                            format_args!("\x1B[{}m", colors.get_color(&record.level()).to_fg_str()),
                        target = record.target(),
                        level = colors.color(record.level()),
                        message = message,
                    ));
                })
                .chain(std::io::stdout()),
        )
        .chain(
            fern::Dispatch::new()
                .format(move |out, message, record| {
                    out.finish(format_args!(
                        "[{target}][{level}] {message}",
                        target = record.target(),
                        level = record.level(),
                        message = message,
                    ));
                })
                .chain(fern::log_file("output.log")?),
        )
        .apply()?;

    let args = Args::parse();

    let resource_dir = if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        path
    } else {
        path::PathBuf::from("./resources")
    };

    // Make a Context.
    let (mut ctx, event_loop) = ContextBuilder::new("Simple Wars", "...")
        .add_resource_path(resource_dir)
        .window_setup(ggez::conf::WindowSetup::default().vsync(false))
        .build()
        .expect("aieee, could not create ggez context!");

    let game = Game::new(&mut ctx).expect("Failed to create game instance");

    // Run!
    event::run(ctx, event_loop, game);
}
