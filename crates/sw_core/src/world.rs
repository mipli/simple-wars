use glam::IVec2;
use serde::Deserialize;
use utils::spritesheet::SpriteKindTrait;

#[derive(Hash, Deserialize, Eq, PartialEq, Clone, Copy, PartialOrd, Ord, Debug)]
pub enum TileKind {
    Grass,
    Water,
    Soldier,
}

impl SpriteKindTrait for TileKind {}

pub struct World {
    pub tiles: Vec<TileKind>,
    pub width: usize,
    pub height: usize,
}

impl World {
    pub fn new(size: usize) -> Self {
        let mut tiles = vec![];
        for x in 0..size {
            for y in 0..size {
                if x == 2 || y == 1 {
                    tiles.push(TileKind::Water);
                } else {
                    tiles.push(TileKind::Grass);
                }
            }
        }

        World {
            tiles,
            width: size,
            height: size,
        }
    }

    pub fn get_all_neigbours(&self, pos: IVec2) -> Vec<(TileKind, IVec2)> {
        let delta: [IVec2; 8] = [
            (-1, -1).into(),
            (0, -1).into(),
            (1, -1).into(),
            (-1, 0).into(),
            (1, 0).into(),
            (-1, 1).into(),
            (0, 1).into(),
            (1, 1).into(),
        ];

        delta
            .iter()
            .filter_map(|&d| {
                let p = d + pos;
                if self.is_valid(p) {
                    let tile = self.get(p);
                    Some((tile, d))
                } else {
                    None
                }
            })
            .collect::<Vec<_>>()
    }

    pub fn is_valid(&self, pos: IVec2) -> bool {
        pos.x >= 0 && pos.y >= 0 && pos.x < self.width as i32 && pos.y < self.height as i32
    }

    pub fn get(&self, pos: IVec2) -> TileKind {
        let index = self.to_index(pos);
        self.tiles[index]
    }

    pub fn revert_index(&self, index: usize) -> IVec2 {
        let mut index = index as i32;
        let y = index / self.width as i32;

        index -= y * self.width as i32;

        let x = index % self.width as i32;

        (x, y).into()
    }

    fn to_index(&self, pos: IVec2) -> usize {
        assert!(pos.x >= 0);
        assert!(pos.y >= 0);
        (pos.y as usize * self.width) + pos.x as usize
    }
}
