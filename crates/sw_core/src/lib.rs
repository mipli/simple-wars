use components::{DrawLayer, Drawable, Position};
use hecs::Entity;
use world::{TileKind, World};

pub mod commands;
pub mod components;
pub mod error;
pub mod world;

pub struct GameWorld {
    pub entities: hecs::World,
    pub world: World,
}

pub struct StateUpdate {
    damage_entity: Entity,
    damage_amount: i32,
}

pub enum ActionResult {
    Success(StateUpdate),
    Failure,
}

impl GameWorld {
    pub fn new(size: usize) -> Self {
        let world = World::new(size);
        let mut entities = hecs::World::new();

        entities.spawn((
            Position {
                coordinate: (0, 0).into(),
            },
            Drawable {
                sprite: TileKind::Soldier,
                layer: DrawLayer::Unit,
            },
        ));

        GameWorld { entities, world }
    }
}
