use crate::world::TileKind;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Position {
    pub coordinate: glam::IVec2,
}
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Health {
    pub current: i32,
    pub max: i32,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum AttackKind {
    Infantry,
    Vehicle,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Attack {
    pub range: u32,
    pub kind: AttackKind,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum UnitKind {
    Infantry,
    Vehicle,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Unit {
    pub name: String,
    pub kind: UnitKind,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum DrawLayer {
    Entity,
    Unit,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Drawable {
    pub sprite: TileKind,
    pub layer: DrawLayer,
}
