use hecs::Entity;

use super::{components, error::SwError, ActionResult, GameWorld, StateUpdate};
use crate::error::SwResult;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct CommandContext {
    entity: Entity,
    change: Change,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Change {
    MoveTo(glam::IVec2),
    Damage((Entity, i32)),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Command {
    Move(Move),
    Attack(Attack),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Move {
    entity: Entity,
    from: glam::IVec2,
    to: glam::IVec2,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Attack {
    attacker_id: Entity,
    target_id: Entity,
}

pub fn execute_command(command: Command, state: &GameWorld) -> SwResult<CommandContext> {
    match command {
        Command::Move(ref move_cmd) => perform_move(move_cmd, state),
        Command::Attack(ref attack_cmd) => perform_attack(attack_cmd, state),
    }
}

fn perform_attack(action: &Attack, state: &GameWorld) -> SwResult<CommandContext> {
    let mut health_query = state.entities.query::<&components::Health>();
    let health_view = health_query.view();

    let mut attack_query = state.entities.query::<&components::Attack>();
    let attack_view = attack_query.view();

    let mut unit_query = state.entities.query::<&components::Unit>();
    let unit_view = unit_query.view();

    let attack = attack_view
        .get(action.attacker_id)
        .ok_or(SwError::MissingEntity)?;
    let attacker_health = health_view
        .get(action.attacker_id)
        .ok_or(SwError::MissingEntity)?;
    let target_unit = unit_view
        .get(action.target_id)
        .ok_or(SwError::MissingEntity)?;

    let base_damage = match (attack.kind, target_unit.kind) {
        (components::AttackKind::Infantry, components::UnitKind::Infantry) => 100.0,
        (components::AttackKind::Infantry, components::UnitKind::Vehicle) => 50.0,
        (components::AttackKind::Vehicle, components::UnitKind::Infantry) => 50.0,
        (components::AttackKind::Vehicle, components::UnitKind::Vehicle) => 100.0,
    };

    let power_multiplier = 0.5 * (attacker_health.current as f32 / attacker_health.max as f32);

    let damage = (base_damage * power_multiplier).floor() as i32;

    Ok(CommandContext {
        entity: action.attacker_id,
        change: Change::Damage((action.target_id, damage)),
    })
}

fn perform_move(move_cmd: &Move, state: &GameWorld) -> SwResult<CommandContext> {
    let mut position_query = state.entities.query::<&components::Position>();

    for (_, components::Position { coordinate }) in position_query.iter() {
        if *coordinate == move_cmd.to {
            return Err(SwError::InvalidCommand);
        }
    }

    Ok(CommandContext {
        entity: move_cmd.entity,
        change: Change::MoveTo(move_cmd.to),
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::components;

    #[test]
    fn test_simple_attack_action() {
        let mut state = GameWorld::new(5);

        let attacker = state.entities.spawn((
            components::Health {
                current: 100,
                max: 100,
            },
            components::Attack {
                range: 1,
                kind: components::AttackKind::Infantry,
            },
        ));

        let target = state.entities.spawn((
            components::Health {
                current: 100,
                max: 100,
            },
            components::Unit {
                name: "Target".to_string(),
                kind: components::UnitKind::Infantry,
            },
        ));

        let command = Command::Attack(Attack {
            target_id: target,
            attacker_id: attacker,
        });

        let context = execute_command(command, &state).unwrap();

        let Change::Damage((entity, damage)) = context.change else {
            panic!("Invalid command result");
        };
        assert_eq!(entity, target);
        assert_eq!(damage, 50);
    }

    #[test]
    fn test_move() {
        let mut state = GameWorld::new(5);

        let entity = state.entities.spawn((components::Position {
            coordinate: (0, 0).into(),
        },));

        let command = Command::Move(Move {
            entity,
            from: (0, 0).into(),
            to: (3, 3).into(),
        });

        let context = execute_command(command, &state).unwrap();

        let Change::MoveTo(coord) = context.change else {
            panic!("Invalid command result");
        };
        assert_eq!(coord, (3, 3).into());
    }
}
