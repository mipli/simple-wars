use std::fmt;

pub type SwResult<T> = Result<T, SwError>;

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum SwError {
    MissingEntity,
    InvalidCommand,
}

impl std::error::Error for SwError {}

impl fmt::Debug for SwError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::MissingEntity => write!(f, "MissingEntity"),
            Self::InvalidCommand => write!(f, "InvalidCommand"),
        }
    }
}
impl fmt::Display for SwError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::MissingEntity => write!(f, "MissingEntity"),
            Self::InvalidCommand => write!(f, "InvalidCommand"),
        }
    }
}
