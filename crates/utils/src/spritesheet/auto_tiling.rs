use serde::Deserialize;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Deserialize)]
pub enum Direction {
    C,
    N,
    E,
    S,
    W,
    NW,
    NE,
    SE,
    SW,
}

impl From<(i32, i32)> for Direction {
    fn from((delta_x, delta_y): (i32, i32)) -> Self {
        let dx = delta_x.signum();
        let dy = delta_y.signum();

        match (dx, dy) {
            (-1, -1) => Direction::NW,
            (0, -1) => Direction::N,
            (1, -1) => Direction::NE,
            (-1, 0) => Direction::W,
            (0, 0) => Direction::C,
            (1, 0) => Direction::E,
            (-1, 1) => Direction::SW,
            (0, 1) => Direction::S,
            (1, 1) => Direction::SE,
            _ => panic!(
                "Converting invalid tuple to direction: {}, {}",
                delta_x, delta_y
            ),
        }
    }
}

impl Direction {
    pub fn is_diagonal(&self) -> bool {
        matches!(
            self,
            Direction::NW | Direction::NE | Direction::SE | Direction::SW
        )
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct AutoTileConfiguration<S> {
    pub target: S,
    pub tiles: Vec<AutoTile>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct AutoTile {
    pub rule: AutoTileRule,
    pub offset_x: f32,
    pub offset_y: f32,
}

#[derive(Debug, Clone, Eq, PartialEq, Deserialize)]
pub struct AutoTileRule(pub Vec<Direction>);
