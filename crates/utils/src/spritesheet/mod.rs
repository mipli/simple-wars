use std::{
    collections::{HashMap, HashSet},
    hash::Hash,
    path::Path,
};

use ggez::graphics;
use glam::IVec2;
use serde::{de::DeserializeOwned, Deserialize};
pub mod auto_tiling;
mod sprites;
use sprites::SpriteInfo;

use self::auto_tiling::{AutoTileRule, Direction};

/// Sprite identifier
#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct SpriteId(pub usize);

/// Define transparency for a sprite
#[derive(Copy, Clone, Debug, PartialEq, Eq, Deserialize)]
pub enum Transparency {
    /// The sprite is opaque
    Opaque,
    /// The sprite contains transparent pixels
    Transparent,
}

/// Information about rendering a sprite form the spritesheet
#[derive(Copy, Clone, Debug)]
pub struct Sprite {
    /// Position of the sprite in the spritesheet
    pub rect: graphics::Rect,
    /// Transparency option for the sprite
    pub transparency: Transparency,
}

pub trait SpriteKindTrait:
    Hash + DeserializeOwned + Eq + PartialOrd + Clone + Copy + std::fmt::Debug + PartialEq
{
}

/// Information for auto tiling
#[derive(Debug, Clone)]
pub struct AutoTile {
    // The sprite to display
    pub id: SpriteId,
    // Rules for displaying this sprite
    pub rule: AutoTileRule,
}

impl AutoTile {
    /// Check if a set of directions matches the auto tile rule
    pub fn is_matching(&self, target: &[Direction]) -> bool {
        if target.is_empty() {
            false
        } else {
            target.iter().all(|d| self.rule.0.contains(d))
        }
    }
}

/// Our spritesheet with information about where to find sprites based on either SpriteId or
/// SpriteKind
#[derive(Clone, Debug)]
pub struct Spritesheet<S: SpriteKindTrait> {
    sprites: HashMap<SpriteId, Sprite>,
    auto_tiles: HashMap<S, Vec<(S, Vec<AutoTile>)>>,
    has_auto_tiles: HashSet<S>,
    kind_sprites: HashMap<S, SpriteId>,
    /// The image base for the Spritesheet
    pub image: graphics::Image,
    /// Size of tiles in the spritesheet
    pub tile_size: f32,
}

impl<S: SpriteKindTrait> Spritesheet<S> {
    /// Create a new Spritesheet based on the given image and sprite information
    pub fn create(image: graphics::Image, sprite_info: Vec<SpriteInfo<S>>, tile_size: f32) -> Self {
        let mut sprites = HashMap::new();
        let mut kind_sprites = HashMap::new();
        let mut auto_tiles: HashMap<S, Vec<(S, Vec<AutoTile>)>> = HashMap::new();
        let mut has_auto_tiles = HashSet::new();

        let image_width = image.width() as f32;
        let image_height = image.height() as f32;

        let sprites_per_row = image_width / tile_size;

        let create_sprite = |offset_x: f32, offset_y: f32, transparency: Transparency| {
            let id = SpriteId(((offset_y * sprites_per_row + offset_x) / tile_size) as usize);
            let sprite_rect = graphics::Rect {
                x: offset_x / image_width,
                y: offset_y / image_height,
                w: tile_size / image_width,
                h: tile_size / image_height,
            };
            let sprite = Sprite {
                rect: sprite_rect,
                transparency,
            };
            (id, sprite)
        };

        for info in &sprite_info {
            let (id, sprite) = create_sprite(info.offset_x, info.offset_y, info.transparency);
            sprites.insert(id, sprite);
            kind_sprites.insert(info.kind, id);

            if let Some(ref auto_tile_conf) = info.auto_tiling {
                let tiles = auto_tile_conf
                    .tiles
                    .iter()
                    .map(|tile| {
                        let (auto_id, auto_sprite) =
                            create_sprite(tile.offset_x, tile.offset_y, info.transparency);
                        sprites.insert(auto_id, auto_sprite);
                        AutoTile {
                            id: auto_id,
                            rule: tile.rule.clone(),
                        }
                    })
                    .collect::<Vec<_>>();
                let entry = auto_tiles.entry(info.kind).or_default();
                entry.push((auto_tile_conf.target, tiles));

                has_auto_tiles.insert(info.kind);
            }
        }

        Self {
            sprites,
            auto_tiles,
            has_auto_tiles,
            tile_size,
            image,
            kind_sprites,
        }
    }

    /// Get information about the given sprite
    pub fn get(&self, id: SpriteId) -> Option<&Sprite> {
        self.sprites.get(&id)
    }

    /// Get SpriteId used for the given `sprite_kind``
    pub fn get_sprite_id(&self, sprite_kind: S) -> SpriteId {
        *self
            .kind_sprites
            .get(&sprite_kind)
            .expect("Could not find sprite")
    }

    // Fetch auto tiling for the given sprite kind
    pub fn get_auto_tile_id(&self, sprite_kind: S, neigbours: &[(S, IVec2)]) -> SpriteId {
        if !self.has_auto_tiles.contains(&sprite_kind) {
            return self.get_sprite_id(sprite_kind);
        }
        let auto_tile_targets = self.auto_tiles.get(&sprite_kind).expect(
            "Should always have auto tile, since HashSet test should avoid filter out ones",
        );
        let Some((auto_tiles, target_kind)) = neigbours
            .iter()
            .find_map(|(neighbour_tile, _)| {
                auto_tile_targets.iter().find(|(tile_target, _)| {
                    tile_target == neighbour_tile
                }).map(|(target, auto_tiles)| Some((auto_tiles, *target)))
            })
            .flatten() else {
            return self.get_sprite_id(sprite_kind);
            };
        let has_target = neigbours
            .iter()
            .filter_map(|(tile, delta)| {
                if *tile != target_kind {
                    None
                } else {
                    let dir: Direction = (delta.x, delta.y).into();

                    if dir.is_diagonal() {
                        let vert = neigbours
                            .iter()
                            .any(|(tile, d)| d.x == delta.x && d.y == 0 && *tile == sprite_kind);
                        let hori = neigbours
                            .iter()
                            .any(|(tile, d)| d.x == 0 && d.y == delta.y && *tile == sprite_kind);
                        if vert && hori {
                            Some(dir)
                        } else {
                            None
                        }
                    } else {
                        Some(dir)
                    }
                }
            })
            .collect::<Vec<_>>();

        let sprite_id = auto_tiles
            .iter()
            .find(|auto_tile| auto_tile.is_matching(&has_target))
            .map(|auto_tile| auto_tile.id)
            .unwrap_or_else(|| self.get_sprite_id(sprite_kind));
        sprite_id
    }
}

/// Create a new Spritesheet based on the supplied files
pub fn from_file<S: SpriteKindTrait>(
    ctx: &ggez::Context,
    sprites: impl AsRef<Path>,
    sprite_info: impl AsRef<Path>,
    tile_size: f32,
) -> Spritesheet<S> {
    use std::io::Read;

    let spritesheet = graphics::Image::from_path(ctx, sprites).unwrap();
    let mut sprite_info_file = ctx.fs.open(sprite_info).unwrap();
    let mut file_data = String::new();
    sprite_info_file.read_to_string(&mut file_data).unwrap();
    let sprite_info = sprites::parse_spriteinfo(&file_data);

    Spritesheet::create(spritesheet, sprite_info, tile_size)
}
