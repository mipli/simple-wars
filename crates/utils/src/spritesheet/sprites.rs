use serde::Deserialize;

use crate::spritesheet::{auto_tiling::*, SpriteKindTrait, Transparency};

#[derive(Debug, Deserialize, Clone)]
pub struct SpriteInfo<S> {
    pub kind: S,
    pub offset_x: f32,
    pub offset_y: f32,
    pub transparency: Transparency,
    pub auto_tiling: Option<AutoTileConfiguration<S>>,
}

pub fn parse_spriteinfo<S: SpriteKindTrait>(data: &str) -> Vec<SpriteInfo<S>> {
    ron::from_str(data).unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::spritesheet::auto_tiling::{AutoTileRule, Direction};

    /// Describe sprite in the world
    #[derive(Debug, PartialEq, Eq, Clone, Copy, Deserialize, Hash)]
    pub enum SpriteKind {
        /// A grass sprite
        Grass,
    }

    #[test]
    fn test_sprite_parse() {
        let info: SpriteInfo<SpriteKind> = ron::from_str(
            "(
                kind: Grass,
                offset_x: 0.0,
                offset_y: 0.0,
                transparency: Opaque,
                auto_tiling: Some((
                    target: Grass,
                    tiles: [(
                        rule: AutoTileRule([N, W]),
                        offset_x: 32.0,
                        offset_y: 32.0,
                    )]
                ))
            )",
        )
        .unwrap();
        assert_eq!(info.kind, SpriteKind::Grass);
        assert_eq!(info.offset_x, 0.0);
        assert_eq!(info.offset_y, 0.0);
        assert_eq!(info.transparency, Transparency::Opaque);
        // assert_eq!(info.auto_tiling, None);
        let Some(auto_tiling) = info.auto_tiling else {
            panic!("Missing auto tiling information");
        };
        assert_eq!(auto_tiling.target, SpriteKind::Grass);
        assert_eq!(auto_tiling.tiles.len(), 1);
        assert_eq!(
            auto_tiling.tiles[0].rule,
            AutoTileRule(vec![Direction::N, Direction::W])
        );
        assert_eq!(auto_tiling.tiles[0].offset_x, 32.0);
        assert_eq!(auto_tiling.tiles[0].offset_y, 32.0);
    }
}
